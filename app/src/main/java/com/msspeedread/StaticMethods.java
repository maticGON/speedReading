package com.msspeedread;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MaticAsus on 12. 01. 2017.
 */

class StaticMethods {
    public static ArrayList<String> convertTextToArray(String fullText) {
        ArrayList<String> wordsList = new ArrayList<>(Arrays.asList(fullText.split("\\s+")));

       /* Log.d("Array of words", "size: " + wordsList.size());
        for (String word : wordsList)
            Log.d("Array of words", word);*/

        return wordsList;
    }

    public static boolean isTextUsable(String text) {
        return  text !=null && text.trim().length()>0;
    }

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
