package com.msspeedread;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView lblPivot;
    private TextView lblWordLeft;
    private TextView lblWordRight;
    private Runnable run;
    private Handler mHandler;
    private EditText txtWordsPerMinute;
    private boolean isStopped = false;
    private int counter = 0;
    private int delayToNextWord;
    private ArrayList<String> wordsList;
    private static final float TEXTSIZE_DEFAULT = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblPivot = (TextView) findViewById(R.id.lbl_word_pivot);
        lblWordLeft = (TextView) findViewById(R.id.lbl_word_left);
        lblWordRight = (TextView) findViewById(R.id.lbl_word_right);
        txtWordsPerMinute = (EditText) findViewById(R.id.txt_number_of_words_per_minute);
        mHandler = new Handler();

        setTextSizeFortextviews(TEXTSIZE_DEFAULT);

        run = new Runnable() {

            @Override
            public void run() {

                if (counter < wordsList.size()) {
                    updateSingleWordTextView(wordsList.get(counter));
                    counter++;
                    if (!isStopped)// prevent unwanted continuation and battery draining
                        mHandler.postDelayed(run, delayToNextWord);
                }
            }
        };
    }

    private void updateSingleWordTextView(String word) {

        clearLabels();
        int pivotLetter = findPivotLetter(word);

        /**
         *  assemble word for all three textviews
         */

        //first part
        if (word.length() >= pivotLetter)
            lblWordLeft.setText(word.substring(0, pivotLetter));
        //pivot
        lblPivot.setText(Character.toString(word.charAt(pivotLetter)));

        //last part
        if (word.length() > pivotLetter) {
            lblWordRight.setText(word.substring(pivotLetter + 1));
        }
    }

    private int findPivotLetter(String word) {
        switch (word.length()) {
            case 0:
            case 1:
                return 0;
            case 2:
            case 3:
            case 4:
            case 5:
                return 1;
            case 6:
            case 7:
            case 8:
            case 9:
                return 2;
            case 10:
            case 11:
            case 12:
            case 13:
                return 3;
            case 38://test for extremly long both sides wide word (or big textsize or wearable screens)
                return 17;
            default:
                return 4;
        }
    }

    private void setTextSizeFortextviews(float textSize) {
        lblWordLeft.setTextSize(textSize);
        lblPivot.setTextSize(textSize);
        lblWordRight.setTextSize(textSize);
    }

    private void clearLabels() {
        lblWordLeft.setText(null);
        lblWordRight.setText(null);
    }

    public void startReading(View view) {
        String fullText = getTextFromSource();
        if (StaticMethods.isTextUsable(fullText)) {
            wordsList = StaticMethods.convertTextToArray(fullText);
            fitTextSizeToScreen(wordsList);
            checkInputNrOfWords(txtWordsPerMinute.getText().toString());
            counter = 0;
            isStopped = false;
            mHandler.postDelayed(run, delayToNextWord);
        } else {
            StaticMethods.showToastLong(this, "Your text is missing or is too short.");
        }
    }

    private void fitTextSizeToScreen(ArrayList<String> wordsList) {

        float textSize = TEXTSIZE_DEFAULT;
        for (String word : wordsList) {
            int pivotLetter = findPivotLetter(word);
            //word- first part
            String wordFirstPart = word.substring(0, pivotLetter);
            if (wordFirstPart.length()>0) {
                while (wordFirstPart != TextUtils.ellipsize(wordFirstPart, lblWordLeft.getPaint(), lblWordLeft.getWidth(), TextUtils.TruncateAt.START)) {//+lblPivot.getWidth()+lblPivot.getWidth()
                    textSize -= 1;
                    setTextSizeFortextviews(textSize);
                }
            }
            //word - last part
            String wordLastPart = word.substring(pivotLetter + 1);
            if (wordLastPart.length()>0) {
                while (wordLastPart != TextUtils.ellipsize(wordLastPart, lblWordRight.getPaint(), lblWordRight.getWidth(), TextUtils.TruncateAt.END)) {//+lblPivot.getWidth()+lblPivot.getWidth()
                    textSize -= 1;
                    setTextSizeFortextviews(textSize);
                }
            }
        }
    }

    private void checkInputNrOfWords(String wordsNr) {
        if (wordsNr.length() > 0 &&
                android.text.TextUtils.isDigitsOnly(wordsNr)) {

            int nrOfWOrds = Integer.valueOf(wordsNr);

            delayToNextWord = getDelayFromWordsNr(nrOfWOrds);

        } else {
            delayToNextWord = getDelayFromWordsNr(getResources().getInteger(R.integer.defaultNrOfWords));
            StaticMethods.showToastLong(this, "Your value is not a number.\n" +
                    "Default value of " + getResources().getInteger(R.integer.defaultNrOfWords) + " words/minute was set.");
        }
    }

    private int getDelayFromWordsNr(int nrOfWOrds) {
        try {
            return 60 * 1000 / nrOfWOrds; //60 s * 1000ms / {user value}
        } catch (ArithmeticException e) {
            StaticMethods.showToastLong(this, "You have entered value 0.\n" +
                    "Default value of " + getResources().getInteger(R.integer.defaultNrOfWords) + " words/minute was set.");
            return getDelayFromWordsNr(getResources().getInteger(R.integer.defaultNrOfWords));
        }
    }

    public void stopReading(View view) {
        isStopped = true;
    }

    /*
    * Method to fetch text from different sources.
    * */
    public String getTextFromSource() {
        return "Svetovni prvak v reliju Sebastiedasdasd Ogier je našel nove delodajalce. " +
                "V novi sezoni bo vozil za ekipo M Sport Ford. Potem ko je Volkswagen še pred " +
                "koncem letošnje sezone napovedal umik iz svetovnega prvenstva v reliju, se bo v " +
                "prihodnji sezoni štirikratni zaporedni svetovni prvak pridružil ekipi, v kateri bo " +
                "tudi dozdajšnji sotekmovalec Estonec Ott Tänak. Ogier je tako dobil tudi lepo " +
                "darilo za 33. rojstni dan, ki ga bo praznoval v soboto. S Volkswagnom je Francoz v " +
                "štirih letih privozil do 38 posamičnih zmag v svetovnem prvenstvu, prevlado nemškega " +
                "proizvajalca pa kronal z naslovi prvaka med letoma 2013 in 2016.\" Veliko novega bo " +
                "v novi sezoni, a se že veselim. Vozili bomo novo generacijo dirkalnikov WRC, zame " +
                "pa bo to nov začetek v novi ekipi,\" je dejal Ogier za spletno stran novih " +
                "delodajalcev.\" Še nikoli nismo šli na reli Monte Carlo v tako močni zasedbi. Če " +
                "rečem, da sem samo vznemirjen pred novo sezono, bi bilo podcenjevalno. Verjamemo, " +
                "da imamo izreden avto, zdaj pa tudi dva odlična voznika. Nikoli nismo skrivali, da " +
                "si želimo sodelovati s Sebastienom, prepričan pa sem, da je naš avto Fiesta WRC " +
                "tisti, v katerem lahko tudi ubrani naslov prvaka\" je poudaril vodja ekipe Malcolm " +
                "Wilson. 45. sezona svetovnega prvenstva se bo začela 20. januarja v Monte Carlu.";
    }
}
